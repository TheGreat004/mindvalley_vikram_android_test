package cl.mindvalley_vikram_android_test.controller.interfaces;

public interface GenericListener {
    <IResponse, IMethod> void processedResult(IResponse iResponse, IMethod iMethod);
}