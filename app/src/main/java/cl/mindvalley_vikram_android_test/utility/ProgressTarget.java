package cl.mindvalley_vikram_android_test.utility;

import android.graphics.drawable.Drawable;

import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.Target;

/**
 * Created by cl-macmini-108 on 7/28/16.
 */
public abstract class ProgressTarget<Z> extends WrappingTarget<Z> {

    public ProgressTarget(Target<Z> target) {
        super(target);
    }

    @Override
    public Request getRequest() {
        return super.getRequest();
    }

    /**
     * Called when the Glide load has started.
     * At this time it is not known if the Glide will even go and use the network to fetch the image.
     */
    protected abstract void onConnecting();

    /**
     * Called when the Glide load has finished either by successfully loading the image or failing to load or cancelled.
     * In any case the best is to hide/reset any progress displays.
     */
    protected abstract void onDelivered();

    /**
     * In case downing of images failed or cancelled
     */
    protected abstract void onError(Drawable errorDrawable);


    @Override
    public void onLoadStarted(Drawable placeholder) {
        super.onLoadStarted(placeholder);
        onConnecting();
    }

    @Override
    public void onResourceReady(Z resource, GlideAnimation<? super Z> animation) {
        onDelivered();
        super.onResourceReady(resource, animation);
    }

    @Override
    public void onLoadFailed(Exception e, Drawable errorDrawable) {
        onError(errorDrawable);
        super.onLoadFailed(e, errorDrawable);
    }

    @Override
    public void onLoadCleared(Drawable placeholder) {
        onDelivered();
        super.onLoadCleared(placeholder);
    }
}