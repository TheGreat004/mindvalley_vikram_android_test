package cl.mindvalley_vikram_android_test.modals;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "self",
        "html",
        "photos",
        "likes",
        "download"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Links {

    @JsonProperty("self")
    private String self;
    @JsonProperty("html")
    private String html;
    @JsonProperty("photos")
    private String photos;
    @JsonProperty("likes")
    private String likes;
    @JsonProperty("download")
    private String download;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The self
     */
    @JsonProperty("self")
    public String getSelf() {
        return self;
    }

    /**
     * @param self The self
     */
    @JsonProperty("self")
    public void setSelf(String self) {
        this.self = self;
    }

    /**
     * @return The html
     */
    @JsonProperty("html")
    public String getHtml() {
        return html;
    }

    /**
     * @param html The html
     */
    @JsonProperty("html")
    public void setHtml(String html) {
        this.html = html;
    }

    /**
     * @return The photos
     */
    @JsonProperty("photos")
    public String getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    @JsonProperty("photos")
    public void setPhotos(String photos) {
        this.photos = photos;
    }

    /**
     * @return The likes
     */
    @JsonProperty("likes")
    public String getLikes() {
        return likes;
    }

    /**
     * @param likes The likes
     */
    @JsonProperty("likes")
    public void setLikes(String likes) {
        this.likes = likes;
    }

    /**
     * @return The download
     */
    @JsonProperty("download")
    public String getDownload() {
        return download;
    }

    /**
     * @param download The download
     */
    @JsonProperty("download")
    public void setDownload(String download) {
        this.download = download;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}