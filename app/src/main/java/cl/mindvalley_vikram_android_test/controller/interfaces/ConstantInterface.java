package cl.mindvalley_vikram_android_test.controller.interfaces;

/**
 * Created by cl-macmini-108 on 7/27/16.
 */
public interface ConstantInterface {
    /**
     * OkHttp connection setup time
     */
    int READ_CONNECT_TIMEOUT = 90;
    /**
     * Glide Cache Size In MB
     */
    int MAX_SIZE_CACHE_GLIDE = 5;

    /**
     * Row margin in recyclerView
     */
    int ROW_SPACE_MARGIN = 10;

    /**
     * CallBack Constants for server in Activity or fragment
     */
    String DOWNLOAD_PINS = "downloadPins";
}
