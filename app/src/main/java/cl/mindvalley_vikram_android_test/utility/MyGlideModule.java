package cl.mindvalley_vikram_android_test.utility;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.module.GlideModule;

import cl.mindvalley_vikram_android_test.controller.interfaces.ConstantInterface;

public class MyGlideModule implements GlideModule {

    private final int SIZE_IN_BYTES = 1048576 * ConstantInterface.MAX_SIZE_CACHE_GLIDE;

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDiskCache(new InternalCacheDiskCacheFactory(context, SIZE_IN_BYTES));
    }

    @Override
    public void registerComponents(Context context, Glide glide) {

    }
}
