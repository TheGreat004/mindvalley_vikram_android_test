package cl.mindvalley_vikram_android_test.controller.api;

import java.util.List;

import cl.mindvalley_vikram_android_test.modals.UserData;
import retrofit2.Call;
import retrofit2.http.GET;

public interface RestApi {
    /**
     * Since, it have only one url, so rather than appeding it to base, it will create a new Url
     * without attaching it to root
     *
     * @param
     * @return
     */
    @GET("./")
    Call<List<UserData>> getData();
}

