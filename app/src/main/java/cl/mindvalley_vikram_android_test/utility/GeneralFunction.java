package cl.mindvalley_vikram_android_test.utility;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import cl.mindvalley_vikram_android_test.R;
import cl.mindvalley_vikram_android_test.controller.api.RetrofitAdapters;
import cl.mindvalley_vikram_android_test.controller.interfaces.GenericListener;
import retrofit2.Response;

public class GeneralFunction {
    /**
     * find and cast a view with activity context
     *
     * @param activity
     * @param id
     * @param <T>
     * @return
     */
    public static <T> T findViewByIdAndCast(Activity activity, int id) {
        return (T) activity.findViewById(id);
    }

    /**
     * fina and cast a view with the help of parent view
     * @param activity
     * @param id
     * @param <T>
     * @return
     */
    public static <T> T findViewByIdAndCast(View activity, int id) {
        return (T) activity.findViewById(id);
    }

    /**
     * show a toast with any context
     * @param message
     * @param context
     */
    public static void showToast(String message, Context context) {
        if (null == context)
            return;
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    /**
     * Hide a view or viewgorup
     * @param view
     */
    public static void hideView(View view) {
        view.setVisibility(View.GONE);
    }

    /**
     * show a view
     * @param view
     */
    public static void showView(View view) {
        view.setVisibility(View.VISIBLE);
    }

    /**
     * get activity from fragment or activity
     * @param object
     * @return activity
     */
    public static Activity getActivity(Object object) {
        if (object instanceof Activity)
            return ((Activity) object);
        else if (object instanceof Fragment)
            return ((Fragment) object).getActivity();
        else
            return null;
    }

    /**
     * parse a retrofit error
     * @param iContext
     * @param throwable
     */
    public static void parserRetrofitError(Object iContext, Throwable throwable) {
        parserRetrofitError(iContext, throwable, null);
    }

    /**
     * Overloaded method with callback functionality to provide error callback
     * @param iContext
     * @param throwable
     * @param callBackName
     */
    public static void parserRetrofitError(Object iContext, Throwable throwable, String callBackName) {
        Context context = getActivity(iContext);
        String error = null;
        if (throwable instanceof IOException) {
            if (throwable instanceof ConnectException || throwable instanceof UnknownHostException)
                error = context.getResources().getString(R.string.str_no_internet);
            else if (throwable instanceof SocketTimeoutException)
                error = context.getResources().getString(R.string.str_socket_time_out);
        } else
            error = context.getString(R.string.str_toast_unkknown_error);

        showToast(error, context);
        if (null != callBackName)
            ((GenericListener) iContext).processedResult(error, callBackName);
    }

    /**
     * Parse retrofit response
     * @param response
     * @param callingMethod
     * @param iContext
     * @param <T>
     */
    public static <T> void parseResponse(Response<T> response, String callingMethod, Object iContext) {
        parseResponse(response, callingMethod, iContext, false);
    }

    /**
     * Parse retrofit response with callback on succes or failure in parsing or else error
     * @param response
     * @param callingMethod
     * @param iContext
     * @param giveException
     * @param <T>
     */
    public static <T> void parseResponse(Response<T> response, String callingMethod, Object iContext, boolean giveException) {
        Context context = getActivity(iContext);
        int code = response.code();
        if (code >= 200 && code < 300)
            if (iContext instanceof GenericListener)
                ((GenericListener) iContext).processedResult(response, callingMethod);
            else
                showToast(response.message(), context);
        else if (code == 401)
            showToast(RetrofitAdapters.parseError(response), context);
        else if (code >= 400 && code < 500) {
            String error = RetrofitAdapters.parseError(response);
            if (giveException) {
                showToast(error, context);
                ((GenericListener) iContext).processedResult(error, callingMethod);
            } else
                showToast(error, context);
        } else if (code >= 500 && code < 600)
            showToast(context.getString(R.string.str_toast_server_error), context);
        else
            showToast(context.getString(R.string.str_toast_unkknown_error), context);
    }

    /**
     * Check for internet connection
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }
}