package cl.mindvalley_vikram_android_test.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import cl.mindvalley_vikram_android_test.R;
import cl.mindvalley_vikram_android_test.ui.fragments.PinBoard;
import cl.mindvalley_vikram_android_test.utility.GeneralFunction;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = GeneralFunction.findViewByIdAndCast(this, R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);

        AppCompatTextView tvTitle = GeneralFunction.findViewByIdAndCast(this, R.id.toolbar_title);
        tvTitle.setText(getString(R.string.str_title_pinBoard));

        FrameLayout frameLayout = GeneralFunction.findViewByIdAndCast(this, R.id.mn_container);
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(frameLayout.getId(), new PinBoard(), null)
                .commit();
    }
}
