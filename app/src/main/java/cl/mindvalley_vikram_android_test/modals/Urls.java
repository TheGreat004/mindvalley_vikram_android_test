package cl.mindvalley_vikram_android_test.modals;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "raw",
        "full",
        "regular",
        "small",
        "thumb"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Urls {

    @JsonProperty("raw")
    private String raw;
    @JsonProperty("full")
    private String full;
    @JsonProperty("regular")
    private String regular;
    @JsonProperty("small")
    private String small;
    @JsonProperty("thumb")
    private String thumb;

    private boolean isCancelled = false;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The raw
     */
    @JsonProperty("raw")
    public String getRaw() {
        return raw;
    }

    /**
     *
     * @param raw
     * The raw
     */
    @JsonProperty("raw")
    public void setRaw(String raw) {
        this.raw = raw;
    }

    /**
     *
     * @return
     * The full
     */
    @JsonProperty("full")
    public String getFull() {
        return full;
    }

    /**
     *
     * @param full
     * The full
     */
    @JsonProperty("full")
    public void setFull(String full) {
        this.full = full;
    }

    /**
     *
     * @return
     * The regular
     */
    @JsonProperty("regular")
    public String getRegular() {
        return regular;
    }

    /**
     *
     * @param regular
     * The regular
     */
    @JsonProperty("regular")
    public void setRegular(String regular) {
        this.regular = regular;
    }

    /**
     *
     * @return
     * The small
     */
    @JsonProperty("small")
    public String getSmall() {
        return small;
    }

    /**
     *
     * @param small
     * The small
     */
    @JsonProperty("small")
    public void setSmall(String small) {
        this.small = small;
    }

    /**
     *
     * @return
     * The thumb
     */
    @JsonProperty("thumb")
    public String getThumb() {
        return thumb;
    }

    /**
     *
     * @param thumb
     * The thumb
     */
    @JsonProperty("thumb")
    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    /**
     * To check whether imag loading is cancelled or not
     *
     * @return
     */
    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
