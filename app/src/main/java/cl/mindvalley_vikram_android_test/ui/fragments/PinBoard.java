package cl.mindvalley_vikram_android_test.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cl.mindvalley_vikram_android_test.R;
import cl.mindvalley_vikram_android_test.controller.api.RetrofitAdapters;
import cl.mindvalley_vikram_android_test.controller.interfaces.ConstantInterface;
import cl.mindvalley_vikram_android_test.controller.interfaces.GenericListener;
import cl.mindvalley_vikram_android_test.modals.UserData;
import cl.mindvalley_vikram_android_test.ui.adapter.APinBoard;
import cl.mindvalley_vikram_android_test.utility.GeneralFunction;
import cl.mindvalley_vikram_android_test.utility.SpacesItemDecoration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by cl-macmini-108 on 7/27/16.
 */
public class PinBoard extends Fragment implements GenericListener, SwipeRefreshLayout.OnRefreshListener {
    private List<UserData> allPins;
    private APinBoard adapter;

    private TextView tvLoadingTitle;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_pinboard, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvLoadingTitle = GeneralFunction.findViewByIdAndCast(view, R.id.loader_tv_text);

        allPins = new ArrayList<>();

        mSwipeRefreshLayout = GeneralFunction.findViewByIdAndCast(view, R.id.fpb_swipe_to_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        /**
         * it won't trigger the circle to animate, so by adding the following line ,make a delay
         * in the UI thread so that it shows the circle animation inside the ui thread.
         */
        if (GeneralFunction.isConnected(getContext()))
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });

        RecyclerView mRecyclerView = GeneralFunction.findViewByIdAndCast(view, R.id.fpb_recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        SpacesItemDecoration decoration = new SpacesItemDecoration(ConstantInterface.ROW_SPACE_MARGIN);
        mRecyclerView.addItemDecoration(decoration);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mRecyclerView.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        adapter = new APinBoard(allPins);
        mRecyclerView.setAdapter(adapter);

        onRefresh();
    }

    @Override
    public void onRefresh() {
        downloadAllPins();
    }

    @Override
    public <IResponse, IMethod> void processedResult(IResponse iResponse, IMethod iMethod) {
        switch (iMethod.toString()) {
            case ConstantInterface.DOWNLOAD_PINS: {
                mSwipeRefreshLayout.setRefreshing(false);
                if (iResponse instanceof Response) {
                    List<UserData> allPins = ((Response<List<UserData>>) iResponse).body();
                    if (allPins.size() < 1 && this.allPins.size() < 1) {
                        tvLoadingTitle.setText(getString(R.string.str_toast_no_data_found));
                        return;
                    }

                    GeneralFunction.hideView((View) tvLoadingTitle.getParent());

                    if (!this.allPins.isEmpty()) {
                        int previousSize = this.allPins.size();
                        this.allPins.clear();
                        adapter.notifyItemRangeRemoved(0, previousSize);
                    }
                    UserData userData = new UserData();
                    userData.setUser(allPins.get(0).getUser());
                    userData.setUrls(allPins.get(0).getUrls());

                    allPins.add(allPins.size() - 1, userData);
                    this.allPins.addAll(allPins);
                    adapter.notifyItemRangeInserted(0, allPins.size());
                } else
                    tvLoadingTitle.setText(iResponse.toString());
            }
            break;
        }
    }

    private void downloadAllPins() {
        RetrofitAdapters.createRetrofitService().getData().enqueue(new Callback<List<UserData>>() {
            @Override
            public void onResponse(Call<List<UserData>> call, Response<List<UserData>> response) {
                GeneralFunction.parseResponse(response, ConstantInterface.DOWNLOAD_PINS, PinBoard.this, true);
            }

            @Override
            public void onFailure(Call<List<UserData>> call, Throwable t) {
                GeneralFunction.parserRetrofitError(PinBoard.this, t, ConstantInterface.DOWNLOAD_PINS);
            }
        });
    }
}
