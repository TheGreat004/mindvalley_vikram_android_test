package cl.mindvalley_vikram_android_test.ui.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;

import java.util.List;

import cl.mindvalley_vikram_android_test.R;
import cl.mindvalley_vikram_android_test.modals.UserData;
import cl.mindvalley_vikram_android_test.utility.CircleTransform;
import cl.mindvalley_vikram_android_test.utility.GeneralFunction;
import cl.mindvalley_vikram_android_test.utility.ProgressTarget;

public class APinBoard extends RecyclerView.Adapter<APinBoard.PinboardViewHolder> {
    public List<UserData> allPins;

    public APinBoard(List<UserData> allPins) {
        this.allPins = allPins;
    }

    @Override
    public PinboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PinboardViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.row_fragment_pins, parent, false));
    }

    @Override
    public void onBindViewHolder(final PinboardViewHolder holder, int position) {
        UserData particularPin = allPins.get(position);

        if (!particularPin.getUrls().isCancelled()) {
            GeneralFunction.showView(holder.imgBtCancel);
            GeneralFunction.showView(holder.progressBar);
            holder.bind(particularPin.getUrls().getThumb(), position);
        }
        else {
            GeneralFunction.hideView(holder.imgBtCancel);
            GeneralFunction.hideView(holder.progressBar);
            holder.imgPins.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
        }
        //holder.bind("https://api.unsplash.com/users/nicholaskampouris/photos");

        Glide.with(holder.itemView.getContext()).load(particularPin.getUser().
                getProfileImage().getSmall()).transform(new CircleTransform(holder.
                itemView.getContext())).placeholder(R.mipmap.ic_launcher).
                error(android.R.drawable.ic_menu_close_clear_cancel).into(holder.imgUser);
        holder.tvUserName.setText(particularPin.getUser().getName());

        Log.e(position + "data is:-", allPins.get(position).getUrls().isCancelled() + "");
    }

    @Override
    public int getItemCount() {
        return allPins.size();
    }

    /**
     * Demonstrates 3 different ways of showing the progress:
     * <ul>
     * <li>Update a full fledged progress bar</li>
     * <li>Update a text view to display size/percentage</li>
     * <li>Update the placeholder via Drawable.level</li>
     * </ul>
     * This last one is tricky: the placeholder that Glide sets can be used as a progress drawable
     * without any extra Views in the view hierarchy if it supports levels via <code>usesLevel="true"</code>
     * or <code>level-list</code>.
     *
     * @param <Z> automatically match any real Glide target so it can be used flexibly without reimplementing.
     */
    private static class MyProgressTarget<Z> extends ProgressTarget<Z> {
        private final ImageButton imageButtonCancel;
        private final ProgressBar progress;
        private final ImageView imageView;

        public MyProgressTarget(Target<Z> target, ProgressBar progress, ImageView imageView, ImageButton imageButtonCancel) {
            super(target);
            this.imageView = imageView;
            this.progress = progress;
            this.imageButtonCancel = imageButtonCancel;
        }

        @Override
        protected void onConnecting() {
            // Log.e("onConnecting", "onConnecting");
            progress.setIndeterminate(true);
            GeneralFunction.showView(progress);
            GeneralFunction.showView(imageButtonCancel);
        }

        @Override
        protected void onDelivered() {
            // Log.e("onDelivered", "onDelivered");
            hideViews();
        }

        @Override
        protected void onError(Drawable errorDrawable) {
            //  Log.e("onError", "onError");
            if (null != errorDrawable)
                imageView.setImageDrawable(errorDrawable);
            hideViews();
        }

        private void hideViews() {
            GeneralFunction.hideView(progress);
            GeneralFunction.hideView(imageButtonCancel);
        }
    }

    public class PinboardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final ProgressTarget<Bitmap> target;
        public ImageView imgPins, imgUser;
        public TextView tvUserName;
        public ProgressBar progressBar;
        private ImageButton imgBtCancel;

        public PinboardViewHolder(View itemView) {
            super(itemView);

            imgBtCancel = GeneralFunction.findViewByIdAndCast(itemView, R.id.rfp_imgBt_cancel);
            imgBtCancel.setOnClickListener(this);

            progressBar = GeneralFunction.findViewByIdAndCast(itemView, R.id.rfp_progressBar);
            imgPins = GeneralFunction.findViewByIdAndCast(itemView, R.id.rfp_img_pins);
            imgUser = GeneralFunction.findViewByIdAndCast(itemView, R.id.rfp_img_user);
            tvUserName = GeneralFunction.findViewByIdAndCast(itemView, R.id.rfp_tv_userName);

            target = new MyProgressTarget<>(new BitmapImageViewTarget(imgPins), progressBar, imgPins, imgBtCancel);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.rfp_imgBt_cancel: {
                    int position = getAdapterPosition();
                    UserData userData = APinBoard.this.allPins.get(getAdapterPosition());
                    userData.getUrls().setCancelled(true);
                    APinBoard.this.allPins.set(position, userData);
                    notifyItemChanged(position);
                    Glide.clear(target);
                }
                break;
            }
        }

        void bind(String url, int position) {
            Glide.with(imgPins.getContext()).load(url).asBitmap().placeholder(R.mipmap.ic_launcher)
                    .centerCrop() // needs explicit transformation, because we're using a custom target
                    .error(android.R.drawable.ic_menu_close_clear_cancel)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .signature(new StringSignature(String.valueOf(position)))
                    .into(target);
        }
    }
}
