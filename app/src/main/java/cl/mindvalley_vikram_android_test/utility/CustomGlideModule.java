package cl.mindvalley_vikram_android_test.utility;

import android.content.Context;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.GlideModule;

import cl.mindvalley_vikram_android_test.controller.interfaces.ConstantInterface;

public class CustomGlideModule implements GlideModule {

    /**
     * Just set the momory size for glide, else everything will be
     * automatically done by glide
     */
    private final int SIZE_IN_BYTES = 1024 * 1024 * ConstantInterface.MAX_SIZE_CACHE_GLIDE;

    /**
     * Set Memory cache for glide
     *
     * @param context
     * @param builder
     */
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setMemoryCache(new LruResourceCache(SIZE_IN_BYTES));
        //builder.setDiskCache(new InternalCacheDiskCacheFactory(context, SIZE_IN_BYTES));
    }

    @Override
    public void registerComponents(Context context, Glide glide) {

    }


}