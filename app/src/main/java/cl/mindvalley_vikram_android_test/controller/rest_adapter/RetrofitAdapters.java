package cl.mindvalley_vikram_android_test.controller.rest_adapter;

import android.support.annotation.NonNull;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

import cl.mindvalley_vikram_android_test.BuildConfig;
import cl.mindvalley_vikram_android_test.controller.interfaces.ConstantInterface;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class RetrofitAdapters {
    private static OkHttpClient.Builder CLIENT;
    private static String ROOT = "http://pastebin.com/raw/wgkJgazE/";

    /**
     * This block will be called only once through out the app, as it contains the configuration of
     * connectivity time for okhttp client.
     */
    static {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.connectTimeout(ConstantInterface.READ_CONNECT_TIMEOUT, TimeUnit.SECONDS);
        builder.readTimeout(ConstantInterface.READ_CONNECT_TIMEOUT, TimeUnit.SECONDS);
        builder.writeTimeout(ConstantInterface.READ_CONNECT_TIMEOUT, TimeUnit.SECONDS);

        CLIENT = builder;
    }

    /**
     * This will generate a adapter for Network Operations
     *
     * @return
     */
    @NonNull
    public static RestApi createRetrofitService() {
        return getRetrofit().create(RestApi.class);
    }

    /**
     * Converter and builder is added to Retrofit, for dynamic parsing
     *
     * @return
     */
    @NonNull
    private static Retrofit getRetrofit() {

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            CLIENT.addInterceptor(interceptor);
        }

        return new Retrofit.Builder()
                .baseUrl(ROOT)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(CLIENT.build())
                .build();
    }

    public static String parseError(Response<?> response) {
        Converter<ResponseBody, String> converter =
                getRetrofit().responseBodyConverter(String.class, new Annotation[0]);

        String error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new String();
        }

        return error;
    }
}
